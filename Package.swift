// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EventMonitor",
    products: [
        .library(
            name: "EventMonitor",
            targets: ["EventMonitor"]),
    ],
    targets: [
        .target(
            name: "EventMonitor",
            dependencies: [])
    ]
)
