# EventMonitor

Provides a Swift wrapper around the event-tap mechanism provided by `CGEvent`, allowing you to monitor and capture input events on a Mac. Use this to monitor mouse and keyboard actions across the entire desktop and any running apps; for example, as part of a macro recorder (if coupled with some other playback mechanism). (Any program using this utility will require permissions in System Preferences.) 

See `class EventMonitor`, in `EventMonitor.swift`, for documentation.
